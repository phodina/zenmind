# ZenMind

![ZenMind board - top](data/zenmind_top_3d.png)
![ZenMind board - bottom](data/zenmind_bottom_3d.png)

Controller board for Sisyphus table - table with plotter mechanism located under the top that drives a steel ball with a magnet in a sand. 

This allows to draw various patterns in the sand to create a mesmerizing art over the time.

Along with the drawing the boards has two connectors for WS2812 based LED strips. Therefore the lines in the sand will get additional depth with playful light shades.

The controller uses Trinamic (now Analog Devices) drivers TMC2208 that provide silent operation modes. Also all settings are done through the bi-directional UART instead of the legacy singals - therefore in idle mode even the current through the motors can be reduced.

For thermal management the board has 2 NTC thermistors that measure the temperature of the motor drivers beside querying the drivers themselves over the digital bus.

The heart of the board is RISC-V ESP32-C6 module that offeres 8 MBits of Flash storage for various patterns and boasts with WiFi and Bluetooth connectivity.

Since the ESP32C6 does not offer the SDIO Host interface (just slave) an SPI NOR Flash memory is attached for non-volatile storage of user data.

There's also a DC/DC converter that covnerters the VBUS (higher than 5V if the stepper motors are operating) down to 5V@3A to supply the WS2812 LEDs.

## Links
- [Sisyphus](https://sisyphus-industries.com/)
- [ZenXY](https://docs.v1e.com/zenxy/)
- [Sand table](https://github.com/rdudhagra/Sand-Table)

## License
This project uses the [CERN Open Hardware Licence Version 2 - Permissive](https://ohwr.org/project/cernohl/-/wikis/Documents/CERN-OHL-version-2).
